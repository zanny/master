package com.example.sqlsyncdemo.test;

import java.util.concurrent.ExecutionException;

import junit.framework.Assert;

import com.example.sqlsyncdemo.Poster;

import android.test.AndroidTestCase;
import android.util.Log;
import android.util.Pair;

public class PosterTest extends AndroidTestCase {
	private final String loginURL = "https://bpsqld.herokuapp.com/login",
			addURL = "https://bpsqld.herokuapp.com/add",
			queryURL = "https://bpsqld.herokuapp.com/query",
			modifyURL = "https://bpsqld.herokuapp.com/modify",
			removeURL = "https://bpsqld.herokuapp.com/remove",

			usr = "guest",
			pwd = "ExtremeMeasures",

			badCreds = "^Invalid Credentials$",			
			badCmd = "^badCommand : .*",
			login = "^Login success at .+ UNIX time$",
			select = "[[7]]",
			success = "^Transaction Success$";
	
	//private Poster post;
	private Pair <String, String> result;
	
	//@Override
	//protected void setUp() {
	//	post = new Poster();
	//}
	
	/* The tests are a cascade of success. Later ones WILL fail if predecessors have
	 * issues, but that is inherent to stateful manipulation of a remote database. 
	 * The best practice is to try to have each test pass independently of others,
	 * but that would require larger and larger repetitions of the same statement
	 * sets that would cause preemptive failure anyway. */
	
	/* All these tests run in parallel by the Android test runner - and locks wouldn't
	 * solve it. Instead, I just wrote one massive test that if it passes everything works.
	 * Obviously, bad practice. However, the alternative was to put the tests in a seperate
	 * class and run them via an AndroidTestCase harness, which might be more correct,
	 * but this isn't production code, and I already wrote all this crap, so lets use it. 
	 * Lesson learned. */
	
	/*
	public void testBadLogin() throws InterruptedException, ExecutionException {
		res = post.execute(loginURL, "usr", "notauser", "pwd", "badpassword").get();
		Log.d("BAD_LOGIN", res.first + " : " + res.second);
		Assert.assertEquals("status", res.first);
		Assert.assertTrue(res.second.matches(badCreds));
	}
	
	public void testNologinCommand() throws InterruptedException, ExecutionException {
		res = post.execute(addURL, "cmd", "CREATE TABLE test (nums int);").get();
		Log.d("BAD_CREATE", res.first + " : " + res.second);
		Assert.assertEquals("status", res.first);
		Assert.assertTrue(res.second.matches(badCreds));
	}
	
	public void testGoodLogin() throws InterruptedException, ExecutionException {
		res = post.execute(loginURL, "usr", usr, "pwd", pwd).get();
		Log.d("GOOD_LOGIN", res.first + " : " + res.second);
		Assert.assertEquals("status", res.first);
		Assert.assertTrue(res.second.matches(login));
	}
	
	public void testGoodAdd() throws InterruptedException, ExecutionException {
		res = post.execute(addURL, "cmd", "CREATE TABLE test (nums int);").get();
		Log.d("GOOD_CREATE", res.first + " : " + res.second);
		Assert.assertEquals("result", res.first);
		Assert.assertTrue(res.second.matches(create));
		
		res = post.execute(addURL, "cmd", "INSERT INTO test VALUES(7);").get();
		Log.d("GOOD_INSERT", res.first + " : " + res.second);
		Assert.assertTrue(res.second.matches(insert));
	}
	
	public void testBadAdd() throws InterruptedException, ExecutionException {
		res = post.execute(addURL, "cmd", "UPDATE test SET nums = 1 WHERE nums = 7;").get();
		Log.d("BAD_ADD", res.first + " : " + res.second);
		Assert.assertEquals("status", res.first);
		Assert.assertTrue(res.second.matches(badAdd));
	}
	
	public void testQuery() throws InterruptedException, ExecutionException {
		res = post.execute(queryURL, "cmd", "SELECT nums FROM test WHERE nums = 7;").get();
		Log.d("GOOD_SELECT", res.first + " : " + res.second);
		Assert.assertFalse(res.second.matches(query));
	}
	
	public void testModify() throws InterruptedException, ExecutionException {
		res = post.execute(modifyURL, "cmd", "UPDATE test SET nums = 1 WHERE nums = 7;").get();
		Log.d("GOOD_DELETE", res.first + " : " + res.second);
		Assert.assertTrue(res.second.matches(update));
	}
	
	public void testRemove() throws InterruptedException, ExecutionException {
		res = post.execute(removeURL, "cmd", "DELETE FROM test WHERE nums = 7;").get();
		Log.d("GOOD_DELETE", res.first + " : " + res.second);
		Assert.assertTrue(res.second.matches(delete));
		
		res = post.execute(removeURL, "cmd", "DROP TABLE test;").get();
		Log.d("GOOD_DROP", res.first + " : " + res.second);
		Assert.assertTrue(res.second.matches(drop));
	}
	*/
	
	public void testEverything() throws InterruptedException, ExecutionException {
		result = new Poster().execute(loginURL, "usr", "notauser", "pwd", "badpassword").get();
		Log.d("BAD_LOGIN", result.first + " : " + result.second);
		Assert.assertEquals("status", result.first);
		Assert.assertTrue(result.second.matches(badCreds));

		result = new Poster().execute(addURL, "cmd", "CREATE TABLE test (nums int);").get();
		Log.d("BAD_CREDS", result.first + " : " + result.second);
		Assert.assertEquals("status", result.first);
		Assert.assertTrue(result.second.matches(badCreds));
		
		result = new Poster().execute(loginURL, "usr", usr, "pwd", pwd).get();
		Log.d("GOOD_LOGIN", result.first + " : " + result.second);
		Assert.assertEquals("status", result.first);
		Assert.assertTrue(result.second.matches(login));
		
		// This is cleanup if any tests after the create fail, this deletes the entire table. It should usually fail.
		new Poster().execute(removeURL, "cmd", "DROP TABLE test").get();
		
		result = new Poster().execute(addURL, "cmd", "CREATE TABLE test (nums int);").get();
		Log.d("GOOD_CREATE", result.first + " : " + result.second);
		Assert.assertEquals("result", result.first);
		Assert.assertTrue(result.second.matches(success));
		
		result = new Poster().execute(addURL, "cmd", "INSERT INTO test VALUES(7);").get();
		Log.d("GOOD_INSERT", result.first + " : " + result.second);
		Assert.assertTrue(result.second.matches(success));
		
		result = new Poster().execute(addURL, "cmd", "UPDATE test SET nums = 1 WHERE nums = 7").get();
		Log.d("BAD_ADD", result.first + " : " + result.second);
		Assert.assertEquals("status", result.first);
		Assert.assertTrue(result.second.matches(badCmd));
		
		result = new Poster().execute(queryURL, "cmd", "SELECT nums FROM test WHERE nums = 7").get();
		Log.d("GOOD_SELECT", '"' + result.second + '"');
		Assert.assertTrue(result.second.compareTo(select) == 0);
		
		result = new Poster().execute(modifyURL, "cmd", "UPDATE test SET nums = 1 WHERE nums = 7;").get();
		Log.d("GOOD_DELETE", result.first + " : " + result.second);
		Assert.assertTrue(result.second.matches(success));
		
		result = new Poster().execute(removeURL, "cmd", "DROP TABLE test;").get();
		Log.d("GOOD_DROP", result.first + " : " + result.second);
		Assert.assertTrue(result.second.matches(success));
	}
}
