/* Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.example.sqlsyncdemo;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Pair;

/** Given the target address and the variable key : value pairs of strings
 * to send to the server, this class will handle networking. It will return
 * the key : value pair of whatever the server gives back, if anything, else
 * it will give an error. It doesn't throw and catches any network issue.
 * 
 * This whole mess *only* exists because Android executes callbacks in the GUI
 * thread and requires networking IO be done asychronously. Though I end up doing
 * it sychronously anyway because the whole app is just to send and recieve from
 * a server.
 */
public class Poster extends AsyncTask<String, Void, Pair<String, String>> {
	
	public static final String site = "https://bpsqld.herokuapp.com";
	
	private static final HttpClient client = new DefaultHttpClient();
	private static final BasicHttpContext context = new BasicHttpContext();
	//private static final BasicCookieStore cookies = new BasicCookieStore();
	private static final String localPrefix= "localError",
			sysPrefix = "systemError",
			warning = "warning",
			error = "error",
			result = "result",
			status = "status";
	
	/** Arguments are TARGET_URL, KEY1, VAL1, KEY2, VAL2, ETC */
	@Override
	protected Pair<String, String> doInBackground(String... params) {
		//context.setAttribute(ClientContext.COOKIE_STORE,cookies);
		HttpPost post = new HttpPost(params[0]);
	    post.setHeader("Content-type", "application/json");
	    post.setHeader("Accept", "application/json");
		// Arbitrary limit - the implementation only ever uses at most 5 args, but we
		// give ourselves a bit of headroom.
		if(params.length > 127)
			return new Pair<String, String>(localPrefix, "Too many key:value pairs, can only accept up to 63.");
		// An even number of arguments means there is a dangling key:value
	    if(params.length % 2 == 0)
			return new Pair<String, String>(localPrefix, "Dangling key without a matching pair as argument.");
	    if(params.length < 3)
			return new Pair<String, String>(localPrefix, "Requires at least one key:value pair to send to server.");
	    // We get a dangling comma, but rather than do the last case loop unroll I just 
	    //leave it, it is valid json, and might get trimmed by JSONObject.
	    String resultStr = "Some args weren't json.";
		try {
			JSONObject json = new JSONObject();
			for(byte i = 1; i < params.length; i +=2)
				json.put(params[i], params[i + 1]);
			//Log.d("JSONOBJ CONTENT", sendJSON.toString());
			post.setEntity(new StringEntity(json.toString()));
			HttpResponse response = client.execute(post, context);
			resultStr = EntityUtils.toString(response.getEntity());
			json = new JSONObject(resultStr);
			/* I declare warning, error, etc as static final strings, though in other languages
			 * it could have been just a #define or alias. */
			if(json.has(warning))
				return new Pair<String, String>(warning, json.getString(warning));
			if(json.has(error))
				return new Pair<String, String>(error, json.getString(error));
			if(json.has(result))
				return new Pair<String, String>(result, json.getString(result));
			if(json.has(status))
				return new Pair<String, String>(status, json.getString(status));
	    // I cut out the specific exception behaviour since it always did the same thing.
	    /*
		} catch (UnsupportedEncodingException e) {
			return new Pair<String, String>(sysPrefix, e.getMessage());
		} catch (ClientProtocolException e) {
			return new Pair<String, String>(sysPrefix, e.getMessage());
		} catch (IOException e) {
			return new Pair<String, String>(sysPrefix, e.getMessage());
	    */
		} catch (JSONException e) {
			return new Pair<String, String>("Async wasn't given JSON, or server didn't return JSON", resultStr);
		} catch(Exception e) {
			return new Pair<String, String>(sysPrefix, e.getMessage());
		}
		return new Pair<String, String>(sysPrefix, "Server didn't reply with a valid map key.");	
	}
}