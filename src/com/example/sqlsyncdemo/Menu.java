/* Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.example.sqlsyncdemo;

import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Menu extends Activity {
	public static final String RESULT_STRING = "RESULT";
	private static final String[] queryStrings = {"ANALYZE", "EXPLAIN", "SELECT", "SHOW", "VALUES"},
			modStrings = {"ALTER", "CLUSTER", "REINDEX", "RESET", "SET", "UPDATE", "VACUUM"},
			addStrings = {"CREATE", "INSERT"},
			removeStrings = {"DELETE", "DROP", "TRUNCATE"};
	private static  final Pattern[] queries = new Pattern[queryStrings.length],
			mods = new Pattern[modStrings.length],
			adds = new Pattern[addStrings.length],
			removes = new Pattern[removeStrings.length];    
	private static final String queryURL = Poster.site + "/query",
			modURL = Poster.site + "/modify",
			addURL = Poster.site + "/add",
			removeURL = Poster.site + "/remove";
	
	private EditText queryText,
		modText,
		addText,
		removeText;
	//private LayoutInflater inflater;
	private String help, 
		text;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		
		//inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		// Talk about verbosity. I do this to have arrays of all the patterns to compare a statement against.
		int counter;
		for(counter = 0; counter < queryStrings.length; ++counter)
			queries[counter] = Pattern.compile('^' + queryStrings[counter], Pattern.CASE_INSENSITIVE);
		for(counter = 0; counter < modStrings.length; ++counter)
			mods[counter] = Pattern.compile('^' + modStrings[counter], Pattern.CASE_INSENSITIVE);
		for(counter = 0; counter < addStrings.length; ++counter)
			adds[counter] = Pattern.compile('^' + addStrings[counter], Pattern.CASE_INSENSITIVE);
		for(counter = 0; counter < removeStrings.length; ++counter)
			removes[counter] = Pattern.compile('^' + removeStrings[counter], Pattern.CASE_INSENSITIVE);
		
		queryText = (EditText) findViewById(R.id.queryText);
		modText = (EditText) findViewById(R.id.modText);
		addText = (EditText) findViewById(R.id.addText);
		removeText = (EditText) findViewById(R.id.removeText);
	}
	
	/* In practice, I'd name this something like errorHandler rather than
	 * say what it uses, but the method name is too good not to use. */
	private void toaster(String content) {
		Toast.makeText(this, content, Toast.LENGTH_LONG).show();
	}
	
	private boolean validStatement(String string, Pattern[] patterns) {

		if(string == null || string.isEmpty()) {
			toaster("You need to enter a command!");
			return false;
		}
		for(Pattern pattern : patterns)
			if(pattern.matcher(string).lookingAt())
				return true;
		toaster("Invalid command entered, press help for a list of valid commands.");
		return false;
	}
	
	private void showResults(String result) {
		Intent intent = new Intent(this, Results.class);
		intent.putExtra(RESULT_STRING, result);
		startActivity(intent);
	}
	
	private void executeCommand(String url, String val) {
		try {
			Pair<String, String> result = new Poster().execute(url, "cmd", val).get();
			if("result".compareTo(result.first) == 0)
				// The modifying commands don't need to show results, just toast the output.
				toaster(result.second);
			else
				toaster("Command failed : \"" + result.first + " : " + result.second + '"');
		} catch (InterruptedException e) {
			toaster("Execution Interrupted : \"" + e.getMessage() + '"');
		} catch (ExecutionException e) {
			toaster("Execution Exception : \"" + e.getMessage() + '"');
		}
	}
	
	/* Rather than update buttons to be clickable if their fields contain text,
	 * I just check if the fields are empty when buttons are pressed. Requires 
	 * fewer updates. 
	 * 
	 * Also, holy repeated code batman */
	public void querySubmit(View v) {
		text = queryText.getText().toString();
		if(validStatement(text, queries))
			try {
				Pair<String, String> result = new Poster().execute(queryURL, "cmd", text).get();
				if("result".compareTo(result.first) == 0)
					// Queries always return something useful, so we display that in a result.
					showResults(result.second);
				else
					toaster("Command failed : \"" + result.first + " : " + result.second + '"');
			} catch (InterruptedException e) {
				toaster("Execution Interrupted : \"" + e.getMessage() + '"');
			} catch (ExecutionException e) {
				toaster("Execution Exception : \"" + e.getMessage() + '"');
			}
	}
	
	public void modSubmit(View v) {
		text = modText.getText().toString();
		if(validStatement(text, mods))
			executeCommand(modURL, text);
	}

	public void addSubmit(View v) {
		text = addText.getText().toString();
		if(validStatement(text, adds))
			executeCommand(addURL, text);
	}

	public void removeSubmit(View v) {
		text = removeText.getText().toString();
		if(validStatement(text, removes))
			executeCommand(removeURL, text);
	}
	
	public void showHelp(View v) {
		/* Old implementation using popup windows. Kind of unintiuitive to close, so I just made it output a
		 *  result activity.
		if(help == null) {
			help = new PopupWindow(inflater.inflate(R.layout.help_popup, null));
			TextView helpText = (TextView) help.getContentView().findViewById(R.id.helpText);
		
			StringBuilder helpBuilder = new StringBuilder(
					// Wish Java supported multi-line strings where you escape the newlines.
					"Input a Postgresql command like you would in an interactive client. None of the multi-statement commands are available, and the server will trim any multi-statement inputs to one command.\n");
			helpBuilder.append("Valid queries:\n");
			for(String s : queryStrings)
				helpBuilder.append('\t' + s + '\n');
			helpBuilder.append("Valid modifications:\n");
			for(String s : modStrings)
				helpBuilder.append('\t' + s + '\n');
			helpBuilder.append("Valid additions:\n");
			for(String s : addStrings)
				helpBuilder.append('\t' + s + '\n');
			helpBuilder.append("Valid removers:\n");
			for(String s : removeStrings)
				helpBuilder.append('\t' + s + '\n');
			helpText.setText(helpBuilder.toString());
		}
		help.showAtLocation(queryText, Gravity.CENTER, 0, 0); */
		
		if(help == null) {
			StringBuilder helpBuilder = new StringBuilder(
					// Wish Java supported multi-line strings where you escape the newlines.
					"Input a Postgresql command like you would in an interactive client. None of the multi-statement commands are available, and the server will trim any multi-statement inputs to one command.\n");
			helpBuilder.append("Valid queries:\n");
			for(String s : queryStrings)
				helpBuilder.append('\t' + s + '\n');
			helpBuilder.append("Valid modifications:\n");
			for(String s : modStrings)
				helpBuilder.append('\t' + s + '\n');
			helpBuilder.append("Valid additions:\n");
			for(String s : addStrings)
				helpBuilder.append('\t' + s + '\n');
			helpBuilder.append("Valid removers:\n");
			for(String s : removeStrings)
				helpBuilder.append('\t' + s + '\n');
			help = helpBuilder.toString();
		}
		showResults(help);
	}
	/** Returns to the login screen, we don't need to clear the current
	 * session cookie because logging in again will generate a new one
	 * server side. finish() prevents us from coming back here again. */
	public void logout(View v) {
		setResult(RESULT_OK, null);
		startActivity(new Intent(this, Login.class));
		finish();
	}
}

/* So I regret not just having one liner of "submit a statement" where the server processes any valid command rather than 
 * having the redundant input forms and such. In practice, you could have permissions per command though, but really all you
 * are doing here is submitting SQL commands anyway. */
