 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

By Matt Scheirer

I felt like adding a simple readme, but the only important info to mention is that
the .keystore here is what I'm using to sign it so I can actually install it on devices,
and the password is sqlsyncdemokeystore. Not very secure right there, but this isn't really
the kind of project that demands extreme measures. 

The other important info is that the guest user / password is "guest" and "ExtremeMeasures" for
the backend server. I didn't implement a way to create new users.. since its a demo. But on that
basis, here is a list of potential improvements to this little app:

1. Implement more human usable ways to interact with the database. It is asinine to just write
  raw sql statements for this thing, and a much more practical implementation would have buttons
  like "new table" and "add row" and "add column" and "find row" etc, and in the background craft
  a statement to send.
2. Implement the aformentioned user backend. It would be a seperate table on the psql server, that
  the app just can't modify directly by any means, just containing user / password pairs.
3. As an extension of the previous, salt and possibly scramble the passwords. Don't just store raw
  passwords in a data store.
4. Refactor the poster usage so it doesn't freeze the ui thread when doing transactions. Easiest
  way would be to have seperate posters for logging in and submitting commands, so they know what 
  intents to generate. Would be nice if Java had delegates or function primitives for that.
5. Data heruristics, an access log, a redundant backup of the db (AWS is great and all, but having
  no deterministic backup under our control isn't the best idea.
6. fail2ban on the login gateway to prevent brute forces or ddos. Make it pretty strict, like 3
  attempts before a 30 minute block.
7. By this point, port the entire thing off Java to qt, because not only is qt 5.1 out, I probably
  have severe psychosis from complaining about missing language features.

~Was a neat experiment, I like Bottle, Android can shove itself. At least I know a lot of the
 "quirks" of Android networking now in the event I write more native Android apps.
