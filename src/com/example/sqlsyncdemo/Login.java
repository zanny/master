/* Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.example.sqlsyncdemo;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

// WARNING: Contains ranting about why Java sucks.

/* I'd have a constructor in here, but I don't trust how Android incites Activities */
public class Login extends Activity {
	// In other languages, this would just be a #define, and not waste the memory space.
	public static final String EXTRA_USERNAME = "com.example.sqlsyncdemo.USERNAME";
	// Return code to finish this activity.
	public static final int REQUEST_LOGOUT = 1;

	private EditText usernameText, 
				passwordText;
	private TextView statusText;

	private static final String loginPage = Poster.site + "/login",
			alphanumeric = "^[a-zA-Z0-9]+$",
			loginSuccess = "^Login success at .+ UNIX time$";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		String username = getIntent().getStringExtra(EXTRA_USERNAME);
		
		// Yay for static typing where you are doing potental (C++) reinterpret_cast's.
		// Also, what kind of dumb application design has it doing SEARCHES every time you
		// create an instance of some "activity?". Generating a UI for something this basic
		// should never involve searching!
		usernameText = (EditText) findViewById(R.id.username);
		if(username != null)
			usernameText.setText(username);
		
		passwordText = (EditText) findViewById(R.id.password);
		statusText = (TextView) findViewById(R.id.status);
	}
	
	// This return will finish this activity if the menu it creates logs out.
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent i) {
		if(requestCode == REQUEST_LOGOUT && resultCode == RESULT_OK)
			finish();
	}

	/** Required function signature to call this from the xml layout. There is one 
	 * order of execution that leads to entering the menu, anything else generates
	 * an error displayed in the textView on the screen. */
	public synchronized void login(View v) {
		String usr = usernameText.getText().toString(), 
				pwd = passwordText.getText().toString();
		if(!usr.matches(alphanumeric))
			statusText.setText("Invalid characters in username, must be alphanumeric");
		else if(!pwd.matches(alphanumeric))
			statusText.setText("Invalid characters in password, must be alphanumeric");
		else {
			// This message will never show up because Dalvak will reorder the poster before it, or something.
			//statusText.setText("Signing in...");
			try {
				/* So this completely defeats the whole reason to use an asyc-task, but we have nothing
				 * to do until it returns anyway. The better way to go about this is to have per-view 
				 * posters that will, on completion, generate intents and progress to the next activity.
				 * One problem is that if you did that, you would need to check to make sure you only have one
				 * async running at a time. If Java had lambdas or delegates, I could just pass in the callback
				 * to enter the next activity... */
				Pair<String, String> result = new Poster().execute(loginPage, "usr", usr, "pwd", pwd).get();
				statusText.setText(null);
				if(result.second.matches(loginSuccess))
					startActivityForResult(new Intent(this, Menu.class), REQUEST_LOGOUT);
				else
					statusText.setText("Login failure, error of: \"" + result.first + " : " + result.second + "\".");
			} catch (InterruptedException e) {
				statusText.setText("Interrupted Exception in request: " + e.getMessage());
			} catch (ExecutionException e) {
				statusText.setText("Execution Exception in request: " + e.getMessage());
			}
		}
	}
}

/* This class and its component entries in strings.xml is by itself as long as the entire bottle server.
 * The bottle server that does security, database management, and acts as an http server.
 * This is 2 editable text boxes in a default themed Android app. */
